import { Connection, State, PacketWriter } from "mcproto"
import { createServer } from "net"

const motds = [
    "100% lag free",
    "2b2t is full",
    "Accept it.",
    "All servers are located in a hut in cuba",
    "Pocket edition is better",
    "Position in queue: 1835",
    "Position in queue: 940",
    "what are you waiting for?",
    "You can never leave.\nIt never ends.",
    "You can never leave this place.",
    "when you're bored in queue",
    "Yes please",
    "Yeah I have made a mistake",
    "whats a 2b2?????",
    "What is a MOTD?",
    "this is a christian server,\ndo not swear",
    "There's always a dupe!",
    "Temp map.",
    "shitty graphics",
    "SEVEN YEARS",
    "Server shutting down in 5 seconds.",
    "Just 5 More Minutes...",
    "It is meaningless.",
    "its only a block game",
    "internet game shithole",
    "<insert meme here>",
    "Go away, we're full.",
    "queue simulator"
].map(x => x.split("\n"))

const restOfResponse = {
    version: { name: "1.12.2", protocol: 340 },
    favicon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAVKUlEQVR42u1ad1RUd7dNTDH5srJSVurLl/IS20uiUqSqIAoqInYUEUSsKCgdpAxFERUE0VERlaYgiIpYsURRBGkyWBgFC4pUK2Dv5539GwaRgOKL6/3zwVpnwdy5d+bufdo+5/LOL7/88s5/sr3TTkA7Ae0EtBPQTkA7Ae0E/BN7Nygo6ANra+uPXF1dP4bZ29v/63WmPBdma2vbka9/H5/V2ne4ubm99ybG17Rm774VAvhLPnR2dv45dece86LikvBLVypTyquu7btSffVAi1ZZ02hlFQq7XFF9oLSsMu3cxcvJBSdOLdm0Zds4JucnENoUfExMvEragUOeO3btkaTs2CVJTkmVbEzeIlkfv1ESHbtesnrNOsnyFaskS8LCJQsWBUv8AgIkHp7eEicnF8lMOzuJzZQpkvETJviMHDna3cjIyLpnz56qbJ/js/9PHmfw32Vl582tv/vwErXh53kze9ZgT9me8IHH/OIRv3j4+DnVXK+9sCMtbdqcOXM+xfchsjJz8iMra25QWUU1XbxUTsXnSumUvJiOF56iYzn5lH4kk9L2/UXbtu+ixE2biUmhlRGRFBK6lObNX0BzPb1pjoMjTZ02nSwmWNKIESMfGvTvn9q9e/eubwze19f3t7LKmj3Uxp9XgW9KwMMnRA+YgHsPnxIT+5hJmIFImD179rdnii+cqqi+RpfLq+h8aRmdKblAJ0+fofyCE5SZnUcH0zNoz94DlJK6gxISk2lddBxJV0ZQcEgo+QfMJ3cPT7Kf7UCTp0wl8/ETaPiIkTRosDH17tMn+Y0IcHJy+v5KVc1eJZg3Ad4UfIve5xf3Hz2juw+e0O17j6i8sqbEzs7uB29vbw0G/6i86ipdKqskTheSnz1PhafklJtfSBlZOXTg0GHatWcfbUlJpQ0JSbRmXTQtk66kRYtDyNcvgFzdPGiW3WyaZDOZxpmPp2HDR9DAQYOob1+96jaD9/Pz+ygnvyDgeSvgXmevBv/C+3fuP0YE0K36u898fHx0t+/c7awI/xq6eJnD/3wpnT5TQgUnTlN27nE6fPQY7TtwiLg+UPKWFIrbkEBcE2hpuJQWLFxMPhI/cnZxJduZs2ii9SQyGzuOhpoOI0OjgSDgdpsJ8A4I+P3ug6fXWgPIOJ6WlJbtPiU/u/zUmZJwhRW/ZIWnzkhLL1fkAnxTAgC+qffr7jygm3V3aWFw8PC8Atm2iioO/ytNw/8s5ctOEtchOnT4aJP830IxsRtoFec/F0SaHxhEnl4+5ODoTNOm25Kl1UQaPcaMhpgMpQEDDJECV9ta8d/LLzjp8ayZJ5sScK60LAXFES0NHaIlY49+snlrqktz7wN8U+/X1t+jazfrnnD+Dy+5cOkKdw5i4oj/pqKz56jwZBGHv0yEf+qOXVdGjRo1kE3d1NRUfeTIkTozZsy0DZgfWNWY/3McaMrUaTTeYgKNGDmKBhsPIYP+A0hbV7egzQRkHMsdfFx2wjcrOz/o4KHDEXv27d968PCR9IOHMzIOHclMj09K8o6KizONi4s3ac02JiXZVF+/VfJy6CvA32XwtwH+9n32/h2u9lcuBgQGTuHcfw7vXyi9QmdLLtKporOK6o/wz8ii9fGJycOGDfuk6f3q6el94ezqtlPi608uru6c//bN8n8w9etnQOoaGmFtTgEIFfT9w5nHfLhVnWEAT5tHxLMWqrwy35VebxH8g8eNoc+5T9dv3aasnNykHXvSll+paOZ9FL/jhXT0WK6o/vMCA+eNGTPmV/b8z8bGxj8bGhr+Nnbs2DE+vn7XWAqQo5MLTZ8xk6wmWtMYs7E0dKipyP/effUqOnfu3L3N4ENCwntV1FzPUoKBPaVXW0vAHz1RVPwHj5TgnzQBf49u1N6m6ms3n4aGhltxmzvKAqsx95t7f99fhyhp89Zb6xMSq9ZGxVRx66vi3n914eKQp37Nwh/9f+So0WQ8xATeL+3eXcWEsX3wWvAxMTEdQkJCenFOngMAJZgnz14AbM1eAt6k2t9/1JLnAf4OXbtRR/mFJ7fPmjVLt+Ti5TouAaLyF3HllzXk/lHOfWXv37Z9pxA/MXEbKGL1GgpbuowWBC1qqP5uNHOWHVlOtH7OoV9vaGiUr6ur69u1a9dOjO39tkjhdxH2HIa5uPGHTxQglICUAFsy5TlNgcPr9/nAPfY6Ct7fwN+so7PnLsq8vLy6r42OtUbbQ+jLi8/TCSF8uPJD+cH7ovWl0eat27j3JxJHALEcFuInYF4geXh6sfpz4uo/Q1n9nw8cZFzFhW8h4/q6TbPAtGnTPsnOk4Xhhu+zx+A5AaQJGU0JgbFnH9fduV/HVgu7dRt2r/Zm/d3aG3V3am/culN77WZ97dUbdbU112/VVl29UctC5xoDPXHoyNHFTPiv+F4WN+sgepShX8Chn51XQBmZ2UL47E7bz16Pv7AmKqZIunJ10ZKwpUXs+fPzAxc+5vxvKH6zyWbyFFZ/FpC/jdWfi18A4+v4OgLeDQoJ0eGCdOcOewpVGvl6TxDRQEYTQmAlFy9luHt59XZ0dOzCLaxTW2z69OmdZsyY8Qu3z6/RQvG9kydP/pYL3WlU/dOs+WUsekToo/A19P1NW7dd58LXizvAN7D+/ft/M2DAgB8tLC29oP0dHJ24+Nm+KH4sfowGDiR9/X6kra1zWhkFrRKAkTVfVhiNtoQwRX9GyN4RRCjJUBCitNj1CU6Qy9Dvb2IADGMyvkTB5RFZkyv+Yyg+9Pw8rvoQPRh6EPo7d6dB7u4fPXr0Z83u+z3uCFbK1tfofe79xuz9/gMGQP2RppZWGZ/7wysJgHeuVFy9itxEa6q9fY+JuM9EvCBDQcijxny+cPnKJZ7Y5JeFVcl5eJFfKq+Us4aXcyuT8yQn54ou59CWl5y/JGcPy4uKz8nZy/LC03I5j8RZTMQPCYmbXBTg5ZTHAw/yHlV//1/pQvNj6JH4+a8yMTH5H259ndn7nfX19bsNHjzYaKadfRYmP0XuN/X+INLn3q/buw/10tAoYozftEoAKj8LjImco8S5yn25XhQpyFMULJChiAxFdIgIafjdaPw+zsH5uA7Xo7+j0LGOoOqrN6my+jpB5UHooOCxts/lCPhl74G/UuF5THsYd4+w3j9wUJH3kLxJyVuJi+T9FRGRdaFLl9UFLVpc5+s/r55l7zMnZ1dR+SF8FLmvUH5K7+vo6JKquvpWxvlJqwQg/I9kZsdV8A1ykaKaayCiVty8gozbdFMQ0kBKE7upNH4f5ylA1wsiQagSOKa7svJqKi2rECqPIwKyNtTS0rJLxrGc8rwm4P86dES0vNQdu8XAg6q/LjqWVqxaTaGs+dH2oPow9dnPnsN9fzpNsLQSut+EhY+RkSL34X0tbe1nf/zxhznj7NAqAVyUvue2U3KZbxAewiyOiQw3DzLgQQBCzwYpAPiy1Yn3r/J5AvS1m1TF14PQ8sqrYrEBgYPlBio9tz5MeM8DAhYY+wYEGHPBe46cx6SHiv8C/DaK35hEUTFxtAo9P3w5LVwUTH7+88hjLrc9Ufhmiqlv7DhzIXsx96Py9+nbV3hfRU0tgTF++co2aGNj8/PpM+duQoTAQwhR3DRuHpMZPAhCEB1VTEp1MxPH+f3KmuuCPHgbkhYLDcz0AH7+YpkQOGe4x6PSZ2XnllpYWPy2fmPiwkyu9sh5hP3L4DdRdMx6iohcS+HLpLQoeAn5c8/HxOfk7EK2IvSniKUHVN8QVn0DDI1IT18f4OtUVFXDGN9/Nd0Lttr/t+9M8+UbyWBP8K+CzMyc/FxuQ4VHsrLlRzKPFfPvYp7GijlcizObWIbS+H1Oo2KeHYoZTPGhjMyz6RmZRfx3IR/P4esz+e9MHqYyGWgGCxpfzv/PE5OSF21N3VGwaXOKLGHjJllsXLyMRY5s1epIWfiyFbLgkDBZ4IKFMl8/f5mHh5fM0clZxjkvs5k8VTZhgqVstJmZbKjpcJnRoEF5+v36/aWj0ztWTU3NgXX/n4ztX23eCjdsaj9HNLjOnWuSmLwlgqtwTlZO3iXu0dVcoGryZSdqjstOKaywwfjvfBi/n1dQWJN7XFaTk19Qcywnr+rI0awLe/buz1q/IVE628nJCBUf3zF16tTPsPsbP378N6zn/YPDwpYHLQ6RzgsMkvoFzJN6SyRS97leUicXV+mcOQ5SW9tZ0ilTpkktJ06SjhtvIR01ZozU1HSYdNAgYyl3BGnfvn2Xa2vrLtPQ0pr/+++/f9+wDX6ztTj32A8cHBxUdqXtSy44cfohwhTTGEL2LKuzYs5bhDCKF+Rqo/FrHMfiEkLmTPEFkvN1aGsnWdFhmsM251jO8TsxGxLmcdH7ouE7O/jPn2+GkMd2Z+u2HVztt2DcpahoRc6HL19Bi1nqYtHhzVofRW82DztYdijzHvs+9PyGhUclf+6/3/i5AMBL/P0H8cBRBgWGdgQpimHkBAPARuZUUbHIXQArEnZO/Mbr0/ISsbUFYGh4tDSAPi47KURNdm6BEDacSs+WSVfNwfchAlZFREagzUHfb0xKprj1CULjY8O7lHN+Mef8vMAFDN63AbyjUHsTrW3ErA/Bg22PoZGRqPqamtq7GM+nb0pABzs7R81de/dXoBBh64Kbxf4th8kAABACMCAFwGRNDK9xHGNrvgB8QshYXI/PwSSH1oZVForc5pTUUwh9yNno2DgZejwWm9GxGyhybRStWBkhJjwsOAPmB5K3z8vgrSfZiH6PomfC4NHysOzo3acvqaioeCqnvjYTwN74igvPPoQiZCfaEG4WZHDxEgC4wD3miaz+cMax+sOZ2fVHYFkNv9m48NXzeU8EYK7oGGBwPT4HPR2KDnoeqo5D/a6pqWlXJkF1ffzGh/A6enzE6rVis7skdCkFLVws1tue3j5iyMGKG2H/Enj0e1Z7/Qz6i5anraPzpFO3brpv+misg6OLyygOwWcpqTtp+8494iZBxt4DB8WNp2zfdY4L5EBuWV3NzMy6tGQMppuPr7+HEjA8DTIBenfaPjHGItS3pGxHqD9g7//p6eljj/4euWYde301LeUej3znii9W2xhwsN21s58jZC5yvin4gQzeQIBntaerC7lb3NDy2k4AcjEkdOlivpGa2PXxNVyArm1I2HQjISn5VuKmzbWbNm+7GbQo2M3c3PxLJuDT1gwhHRIWFgziAHjn7r2CTIDeum276OnY4ELURK6LrkIEsJyNX7kqUhQ6PNHh7xFzPfIdmx1ud2LAgcrDhIecbw5eSF3d3lB7pKqqGsuYPn7TCHiXb+YLtl+HDBmiya3GiUNv1+IloQVLQsNPhoYvL2T5eThyXVQaT2Npa6Ki09ZFxaZxyL4wfr02OiaDvfsIQwu8jKK2afNWsbmBmOFQFxscPpeCl4SmGxoafu3s7j7C2dnVfbaDg4ftrFke06fbzp1oY7MZIY98nzFzlniyg+UGqr2y4HG+n9bU1PRRV9fwUFVV9+jRQ9WjZ8+eLtz+1JRy941qAAP/0NzSsnfQwuDDrLieowpDfXFEiBtHgeJpTVRpAEKrQuF6YVuEdzcmbRaPqeBltDIelcUzO+Q3ihvamnRFBLl5ePgj8hputvEpbp8+fT62tLIKx1IDIW89abLY62G6G84SF7s9LDe1tLTmNSw3XvkUuE0EADyH8DDOuxo8WEDrwZoJQweKUuSaKPHYCSCQrwAETyosvvFvHI+KUYAFgQAM+Yp2JuXPQ5jzFIcwr+fvVGnpxpiALznUM+F1ZciPGj2GTIcNF9MdJC4rvec82AxoK9jXEfCesalpX3dPrxpsVCE2UH2ht5GTuGEQgsoMEChUyFmAgjcbLUJxDKThAeVyPp8lLLey5aKio7BhgEFxm+PgJGUPftLSjamrq3fjcK+3sLAkM7NxYqwV+T5oMI+2hqzv+6HSX+Fzf3or/yDBevnf02bMzMMmFUUHAwY2q5i0AuYvEDcMQhZyP4YgCV4SJogBKERLo/FrHMf7AIv+jYIWyCMrHlWjnWF0dXB0TtPR0fmpFe91MDAwsECuj+JCB69D3SHkRbHT01csNnppbH+d0GkrAR2HjRjhh96KZQL6rANXXWdXN3JznytakJe3RBAi4ZYEUvj3XRYll1mSlnr7NjOJb6mXt2+pp5eklK8tdZ/rWerqPrfUycXtnP0ch4NWVtaOGhoa370idDsaDRwYgXEWhU7pdQ556sMCB5VeW1uHevToMfd1QqdNBHTv3r2L2dhxVaiwKDZ4kACVBTLQd7FiwhMWRIeLmzvNtLPP6WNgoKmrq/sj5+oPbTWA5vb02etu+s8///yKvX0cszxyvZ/wuh71Zq9jptfS0iYNTc2nnTp16/M2/kfoA319fU+0FWxQEHZ4iKggw4Z4zBT/WQFC8IiZwZOJqak/5+5/M5gfQUJzw/FXWYP3P2ztpriwdWeP30O463G49xZe1xVe19TSYvBapKaufl652PynBHzFDGejsqLIIOxGNiEDagvtB4RAfWHXxq+v8+tyq4mThE2cZNNoVjA+hvdhPKOXM6Hl/DnlZuPMy/lzyzmvS1in/9zKTXXgejQFSwyEu25DuMPrmpoAr0kaGpqs89WSlXu9f0RA586dVfrq9XuAtZEhhxvyTUHGUFF8MF5CcYEQVGOQgpYEYhApIKe54fj48RPI3NxCnI9/SsD1o0aNUQiYISZFLFS+a+WmPu7VSzNGANf5O3Clcdo6vGrObzMBPVRU7HV79xZsI9wwSfVnMpB7GC6Qh8ZDhohihPUySEGUgBiAgYGgRms4hvchWHA+rkN0QbwM5s/T09OLeoVM/ZZ1/OnWgMO4+j/u1KmTxlv5P0EOt0RoZ7CNPEOhARloNZipUYAUhBiKNgRSECUgBmAQLX8zPo73cR7Ox3UgFKtp5DX3eKvWZCpHpDqDfNQScKWpqqvL+dzv3hIB6ntRVMA2WG8kg6stwrCREB409BpIwQOGfgYGghwAMujfX2xeFb8Vx/AeognnI59RxTGm8mfWdunSpVsrN/TeHz162LUGXGk9e6rGtWXQaRMBnEvDeIjYp6qmlg5TE9YrXa1Xr/RewjTSOSTT+YvT2SvpmpraCtPWTtcWptOCKd4XxufiOlzPn3OIvRf2CvHyPhdHe3bKMb6nnJaM38viNjm8LYNOW7tAh4aW1PH/yV4nXN5vOO+jVqzj2wLf/s/S7QS0E9BOQDsB7QS0E9BOQDsB7QS0E/Cfa/8LdiwL05+W/kkAAAAASUVORK5CYII="
}

function getStatusResponse() {
    const motd = motds[(motds.length * Math.random()) | 0]

    const extra = [
        { color: "gray", bold: true, italic: true, text: "2B " },
        { color: "gold", text: `${motd[0]}\n` },
        { color: "gray", bold: true, italic: true, text: "2T " }
    ]

    if (motd.length == 2) extra.push({ text: motd[1], color: "gold" })

    return {
        description: { extra, text: "" },
        players: { online: (Math.random() * 1000) | 0, max: 1000 + (Math.random() * 1000) | 0 },
        ...restOfResponse
    }
}

async function handleStatus(client: Connection) {
    client.onPacket = packet => {
        if (packet.id == 0x0) {
            client.send(new PacketWriter(0x0).writeJSON(getStatusResponse()))
        } else if (packet.id == 0x1) {
            client.send(new PacketWriter(0x1).write(packet.read(8)))
        }
    }
}

const getTabHeader = (pos: number, time: string) => ({text: `\n§7§o§l2BUILDERS§r\n§7§o§l2TOOLS     §r\n\n§62b2t is full\n§6  Position in queue: §l${pos}\n§6       Estimated time: §l${time}\n`})
const tabFooter = { text: "\n     §6You can now donate to stay longer in queue, please visit donate.2b2t.org     \n\n§7contact: 2b2t.org@gmail.com\n§7discussion: reddit.com/r/2b2t/\nwebsite: 2b2t.org (currently down)\nthese are the only official 2b2t websites and contacts.\n" }

const clients = new Set<Connection>()

async function handleLogin(client: Connection) {
    const username = (await client.nextPacketWithId(0x0)).readString()

    if (client.protocol != 340) return client.disconnect({ text: "Version not compatible" })
    clients.add(client)

    // login success
    client.send(new PacketWriter(0x2)
        .writeString("00000000-0000-0000-0000-000000000000")
        .writeString(username))
    // join game
    client.send(new PacketWriter(0x23)
        .writeInt32(0).writeUInt8(3).writeInt32(1).writeUInt16(0)
        .writeString("flat").writeBool(true))
    // player look and position
    client.send(new PacketWriter(0x2f)
        .write(Buffer.alloc(8 * 3 + 4 * 2 + 2)))

    let intervals: any[] = []
    let position = 5 + (Math.random() * 500) | 0

    client.send(new PacketWriter(0xf).writeJSON({ text: "2b2t is full", color: "gold" }).writeInt8(0))

    // chat message
    intervals.push(setInterval(() => {
        client.send(new PacketWriter(0xf).writeJSON({
            text: "Position in queue: ", color: "gold", extra: [{ text: position.toString(), bold: true }]
        }).writeInt8(0))
    }, 10000))

    // tab info
    intervals.push(setInterval(() => {
        let n = 0, m = 0.5 + position / 400
        if (Math.random() < 0.03) n = Math.random() ** 2 * 4 * m
        else if (Math.random() < 0.1 * m) n = 1
        position = Math.max(position - n | 0, 0)

        if (Math.random() < 0.002) {
            if (Math.random() < 0.5) {
                client.send(new PacketWriter(0xf).writeJSON({ text: "You have lost connection to the server", color: "gold" }).writeVarInt(0))
            }
            return intervals.forEach(interval => clearInterval(interval))
        }

        if (position < 50 && Math.random() < 0.005) {
            if (Math.random() < 0.5) {
                client.send(new PacketWriter(0xf).writeJSON({ text: "Connecting to the server", color: "gold" }).writeVarInt(0))
            }
            if (Math.random() < 0.5) {
                client.send(new PacketWriter(0xf).writeJSON({ text: "2b2t is full", color: "gold" }).writeVarInt(0))
            }
            return position = 50 + (Math.random() * 400) | 0
        }

        if (position == 0) {
            client.disconnect({ text: "You have lost connection to the server", color: "gold" })
            return
        }

        if (position <= 3 && n > 0) {
            client.send(new PacketWriter(0x19)
            .writeString("entity.experience_orb.pickup").writeVarInt(0)
            .write(Buffer.alloc(4 * 3)).writeFloat(1).writeFloat(1))
        }

        client.send(new PacketWriter(0x4a)
            .writeJSON(getTabHeader(position, "Infinity"))
            .writeJSON(tabFooter))
    }, 1000))

    client.send(new PacketWriter(0x2e).writeVarInt(0).writeVarInt(1)
    .write(Buffer.alloc(16)).writeString(username)
    .writeVarInt(0).writeVarInt(0).writeVarInt(0)
    .writeBool(false))

    client.onPacket = packet => {
        if (packet.id == 0x2) {
            const text = packet.readString()
            clients.forEach(client => {
                client.send(new PacketWriter(0xf)
                    .writeJSON({ translate: "chat.type.text", with: [{ text: username }, { text }] })
                    .writeInt8(0))
            })
        }
    }

    client.onClose = () => {
        clients.delete(client)
        intervals.forEach(interval => clearInterval(interval))
    }
}

createServer(async socket => {
    const client = new Connection(socket, { isServer: true })
    client.onError = console.error

    await client.nextPacket()

    console.log(`${socket.remoteAddress} ${client.state}`)

    if (client.state == State.Status) handleStatus(client)
    else if (client.state == State.Login) handleLogin(client)
    else client.disconnect()
}).listen(25565)
